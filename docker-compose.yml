version: '3'

services:
  zookeeper:
    image: debezium/zookeeper:1.2
    ports:
     - 2181:2181
     - 2888:2888
     - 3888:3888
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000
  kafka:
    image: debezium/kafka:1.2
    ports:
     - 9092:9092
    links:
     - zookeeper
    environment:
     - ZOOKEEPER_CONNECT=zookeeper:2181
    # environment:
    #   KAFKA_BROKER_ID: 1
    #   KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
    #   KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
    #   KAFKA_INTER_BROKER_LISTENER_NAME: PLAINTEXT
    #   KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:29092,PLAINTEXT_HOST://localhost:9092
    #   KAFKA_AUTO_CREATE_TOPICS_ENABLE: "true"
    #   KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
    #   KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
    #   KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
    #   # -v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v
    #   # Useful settings for development/laptop use - modify as needed for Prod
    #   # This one makes ksqlDB feel a bit more responsive when queries start running
    #   KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: 100

  mysql:
    image: debezium/example-mysql:1.2
    ports:
     - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: debezium
      MYSQL_USER: mysqluser
      MYSQL_PASSWORD: mysqlpw

  connect-distributed:
    build:
      context: ./distributed
      dockerfile: Dockerfile
    ports:
      - 8083:8083
    depends_on:
      - kafka
    
    environment:
      CONNECT_BOOTSTRAP_SERVERS: "kafka:9092"
      CONNECT_REST_PORT: 8083
      CONNECT_GROUP_ID: compose-connect-group
      CONNECT_CONFIG_STORAGE_TOPIC: docker-connect-configs
      CONNECT_OFFSET_STORAGE_TOPIC: docker-connect-offsets
      CONNECT_STATUS_STORAGE_TOPIC: docker-connect-status
      CONNECT_KEY_CONVERTER: io.confluent.connect.avro.AvroConverter
      CONNECT_KEY_CONVERTER_SCHEMA_REGISTRY_URL: 'http://schema-registry:8081'
      CONNECT_VALUE_CONVERTER: io.confluent.connect.avro.AvroConverter
      CONNECT_VALUE_CONVERTER_SCHEMA_REGISTRY_URL: 'http://schema-registry:8081'
      CONNECT_INTERNAL_KEY_CONVERTER: "org.apache.kafka.connect.json.JsonConverter"
      CONNECT_INTERNAL_VALUE_CONVERTER: "org.apache.kafka.connect.json.JsonConverter"
      CONNECT_REST_ADVERTISED_HOST_NAME: "kafka-connect-cp"
      CONNECT_LOG4J_ROOT_LOGLEVEL: "INFO"
      CONNECT_LOG4J_LOGGERS: "org.apache.kafka.connect.runtime.rest=WARN,org.reflections=ERROR"
      CONNECT_CONFIG_STORAGE_REPLICATION_FACTOR: "1"
      CONNECT_OFFSET_STORAGE_REPLICATION_FACTOR: "1"
      CONNECT_STATUS_STORAGE_REPLICATION_FACTOR: "1"
      CONNECT_PLUGIN_PATH: '/usr/share/java,/usr/share/confluent-hub-components/,/connectors/'
    depends_on:
      - kafka
      - schema-registry
    volumes:
      - $PWD/creds/gcp_creds.json:/root/creds/gcp_creds.json
      - /u01/connectors/:/u01/connectors/
    command: 
      - bash 
      - -c 
      - |
        echo "Installing connector plugins"
        confluent-hub install --no-prompt debezium/debezium-connector-mysql:0.9.4
        confluent-hub install --no-prompt wepay/kafka-connect-bigquery:latest
        confluent-hub install --no-prompt jcustenborder/kafka-connect-spooldir:2.0.46
        #
        echo "Launching Kafka Connect worker"
        /etc/confluent/docker/run & 
        ### Install elasticsearch connect source
        mkdir -p /usr/share/java/connect-elastic-source
        curl -L --output /usr/share/java/connect-elastic-source/elastic-source-connect-1.1-jar-with-dependencies.jar \
              https://github.com/DarioBalinzo/kafka-connect-elasticsearch-source/releases/download/v1.1/elastic-source-connect-1.1-jar-with-dependencies.jar
        #
        sleep infinity
      
  schema-registry:
    image: confluentinc/cp-schema-registry
    ports:
     - 8081:8081
    environment:
      SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS: PLAINTEXT://kafka:9092
      SCHEMA_REGISTRY_HOST_NAME: schema-registry
    links:
     - kafka

  ksql-server:
    image: confluentinc/ksqldb-server:0.11.0
    hostname: ksql-server
    container_name: ksql-server
    depends_on:
      - kafka
      - connect-distributed
    ports:
      - "8088:8088"
    environment:
      KSQL_LISTENERS: http://0.0.0.0:8088
      KSQL_BOOTSTRAP_SERVERS: kafka:9092
      KSQL_KSQL_LOGGING_PROCESSING_STREAM_AUTO_CREATE: "true"
      KSQL_KSQL_LOGGING_PROCESSING_TOPIC_AUTO_CREATE: "true"
      KSQL_KSQL_CONNECT_URL: http://connect-distributed:8083
      KSQL_KSQL_SCHEMA_REGISTRY_URL: http://schema-registry:8081

  ksql-cli:
    image: confluentinc/ksqldb-cli:0.11.0  
    container_name: ksql-cli
    depends_on:
      - ksql-server
    entrypoint: /bin/sh
    tty: true
