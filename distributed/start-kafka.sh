#!/bin/bash -e

# connector start command here.
exec "/usr/bin/connect-distributed" "/etc/kafka/connect-distributed.properties"